import random
import pygame

import dungeon_types

def random_area_name(type):
	return type
	
#def create_rooms(amount, size_range = [[8,8],[18,18]] ):
def create_rooms(amount_range, size_range = [[8,8],[16,16]] ):
	rooms = []
	amount = random.randint(amount_range[0],amount_range[1])
	while len(rooms) < amount:
		x = random.randint(size_range[0][0], size_range[1][0])
		y = random.randint(size_range[0][1], size_range[1][1])
		r = pygame.Rect(0,0,x,y)
		n = "Room_"+str(len(rooms))
		connections = {}
		parent = None
		new_room = dungeon_types.simple_room(	r,
												n,
												connections,
												parent)
		rooms.append(new_room)
	return rooms
	
def random_size(ranges):
	x = random.randint(ranges[0], ranges[1])
	y = random.randint(ranges[2], ranges[3])
	return [x,y]