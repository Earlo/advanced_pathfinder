import pygame
import os
from random import randint
import itertools
import operator

import grid_tile
import environment
import terrain_generation
import dungeon_generation


class level(object):
	def __init__(self,blueprint_name):
		
		self.min_node_size = 10
		self.average_node_size = self.min_node_size * 5
		self.max_node_size = self.average_node_size * 1.1
		
		# = os.path.join("stages", blueprint_name + ".bmp")
		
		
		#blueprint =  terrain_generation.create_level()
		blueprint =  dungeon_generation.create_dungeon()
		print "OK"
		self.tile_size = 20
		
		w = blueprint.get_width() * self.tile_size
		h = blueprint.get_height() * self.tile_size
		self.size = [ w , h ]		
		
		self.surf_bgr = pygame.Surface( self.size ) #surface for background
		self.surf_obj = pygame.Surface( self.size ) #surface for objects
		self.surf_eff = pygame.Surface( self.size ) #surface for effects
		self.surf_dar = pygame.Surface( self.size ) #surface for shadows and FoW
		

		self.surf_bgr.fill( (255,255,255) )
		self.surf_obj.fill( (255,255,255) )
		self.surf_eff.fill( (255,255,255) )
		self.surf_dar.fill( (55,55,55) )
		self.surf_obj.set_colorkey( (255,255,255) )
		self.surf_eff.set_colorkey( (255,255,255) )
		self.surf_dar.set_colorkey( (255,255,255) )
		
		
		self.LevelRect = pygame.Rect( 0,0,w,h )
		
		self.dimensions = [  blueprint.get_width() ,  blueprint.get_height() ]
		
		self.items = []

		self.tiles = []
		for x in range(0 , blueprint.get_width() ):
			self.tiles.append([])
			for y in range(0 , blueprint.get_height() ): #setting walls
				if blueprint.get_at(( x , y)) == (0,0,0):
					self.tiles[x].append( grid_tile.tile( self.tile_size, [x,y], self,  FLAGS = ["WALL"], width = 0 ))
				else:
					self.tiles[x].append( grid_tile.tile( self.tile_size, [x,y], self, width = 1, colour = blueprint.get_at(( x , y)) ))
					
		#print len(self.tiles),len(self.tiles[0])
		
	def blit_level(self,rect):
		from game import PROGRAM
		PROGRAM.MainWindow.blit(self.surf_bgr,self.LevelRect,area=rect)
		PROGRAM.MainWindow.blit(self.surf_obj,self.LevelRect,area=rect)
		PROGRAM.MainWindow.blit(self.surf_eff,self.LevelRect,area=rect)
		PROGRAM.MainWindow.blit(self.surf_dar,self.LevelRect,area=rect)
		
		
	def connections(self):
		for column in self.tiles:
			for tile in column:
				tile.connections()
	def draw(self):
		for column in self.tiles:
			for tile in column:
				pygame.draw.rect(self.surf_bgr,tile.colour,tile.rect,tile.width)
				
	def create_rooms(self):
		
		#create initial nodes
		print "\n\nCreating nodes to cover entire map...",
		self.nodes = []
		
		for column in self.tiles:
			for tile in column:
				if tile.node == None and "WALL" not in tile.FLAGS:
					
					open_edges = []
					new_node = grid_tile.node(colour = (randint(0,255),randint(0,255),randint(0,255)) )
					self.nodes.append(new_node)
					
					new_node.tiles.append(tile)
					
					tile.node = new_node
					
					for adj in tile.next.values():
						if not "WALL" in adj.FLAGS:
							new_node.edges.append(adj)
							if adj.node == None:
								open_edges.append(adj)
																											
					#while (len(new_node.tiles) < self.min_node_size and len(open_edges) > 0 ): #and ( len(open_edges)/float(len(new_node.tiles)) > .1
					while ( len(open_edges)/float(len(new_node.tiles)**2) > .5 and len(new_node.tiles) < self.min_node_size ):
						
						e_tile = open_edges[0]

						new_node.tiles.append(e_tile)	
						e_tile.node = new_node
						open_edges.pop(0)
						
						for adj in e_tile.next.values():
							if not ("WALL" in adj.FLAGS or adj in new_node.tiles or adj in new_node.edges):
								new_node.edges.append(adj)
								if adj.node == None:
									open_edges.append(adj)						
						new_node.edges.remove(e_tile)
					
		print "...Done!\n",
		
		check = self.nodes[:]

		
		print "merging nodes... from", len(self.nodes),	
			
		while len(check) > 0: #merges too small nodes to bigger ones

			check.sort(cmp=None, key=lambda n: len(n.tiles))

			node = None
			while node == None:
				if len(check[0].tiles) > 0:
					node = check[0]
				else:
					check.pop(0)
					
			if len(node.tiles) < self.average_node_size:
				merge_candidates = []
				for edge in node.edges:
					if not edge.node in merge_candidates and not edge.node == node:
						merge_candidates.append(edge.node)
						
				if len(merge_candidates) > 0:
					
					#finds the node merged node shares most border with
					maxi = 0
					for can in merge_candidates:
						x = 0
						for edge in can.edges:
							if edge.node == node:
								x += 1
						new_value = x/(float(len(can.tiles))**2)
						if new_value > maxi:
							maxi = new_value
							smallest_closest = can
					
					#take the smalles nearby node
					#smallest_closest = min (merge_candidates, key=lambda candidate:len(candidate.tiles))
					if len(node.tiles) + len(smallest_closest.tiles) < self.max_node_size:
						for tile in node.tiles:
							smallest_closest.tiles.append(tile)
							tile.node = smallest_closest
							if tile in smallest_closest.edges:
								smallest_closest.edges.remove(tile)
							
						for edge in node.edges:
							if not (edge in smallest_closest.tiles or edge in smallest_closest.edges):
								smallest_closest.edges.append(edge)
						self.nodes.remove(node)
			check.remove(node)
		
		self.nodes.sort(cmp=None, key=lambda n: len(n.tiles))

		done = False
		while not done:
			if len(self.nodes[0].tiles) > 0:
				done = True
			else:
				self.nodes.pop(0)
				
		mini = len(min (self.nodes, key=lambda candidate:len(candidate.tiles)).tiles)
		maxi = len(max (self.nodes, key=lambda candidate:len(candidate.tiles)).tiles)
		
		print "...to",len(self.nodes),". Largest node being",maxi, "tiles and smallest",mini,"\n"
		
		print "Refreshing node borders\n"

		#refresh borders
		for node in self.nodes:
			#print len(node.tiles)
			node.edges = []
			for tile in node.tiles:
				for adj in tile.next.values():
					if not ("WALL" in adj.FLAGS or adj in node.tiles or adj in node.edges):
						node.edges.append(adj)					
				
		print "Almost done! Connecting nodes to each other!\n"
			
		#connect nodes to each other
		for node in self.nodes:
			node.defined_edges = {}

			#debug 2
			#for tile in node.tiles:
			#	tile.colour = node.colour
			
			for tile in node.edges:
				if not tile.node in node.connections:
					node.defined_edges[tile.node] = []
					node.connections.append(tile.node)
				node.defined_edges[tile.node].append(tile)
				#debug 1
				#tile.width = 0
		
		#debug 2
		#for tile in self.nodes[0].edges:
		#	tile.width = 0
			
			#print len(node.tiles)
		#print len(self.nodes)
					
					
def most_common_value(L):
  # get an iterable of (item, iterable) pairs
  SL = sorted((x, i) for i, x in enumerate(L))
  # print 'SL:', SL
  groups = itertools.groupby(SL, key=operator.itemgetter(0))
  # auxiliary function to get "quality" for an item
  def _auxfun(g):
    item, iterable = g
    count = 0
    min_index = len(L)
    for _, where in iterable:
      count += 1
      min_index = min(min_index, where)
    # print 'item %r, count %r, minind %r' % (item, count, min_index)
    return count, -min_index
  # pick the highest-count/earliest item
  return max(groups, key=_auxfun)[0]		
					
					
					
					
					
					
					
					
					
				