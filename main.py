# coding: utf-8
#This is the main file. Run this, to run the program

import pygame, sys
import game

#Setup
pygame.init()
pygame.font.init()
	
if __name__ == "__main__":
	game.start()
	sys.exit()