def A_star_accurate(s,e):
	
	openset = [] # The set of tentative nodes to be evaluated, initially containing the start node
	closedset = []
	
	#s.g_score = 0   #g score, distance travelled
	g_score = {}
	f_score = {}
	came_from = {}
	
	g_score[s] = 0
	f_score[s] =  get_h_score(s.pos,e.pos) + g_score[s] # f score, how desirable node is to have in the path
	came_from[s] = None
	# Cost from start along best known path.		
	openset.append(s)
	current = s
	
	while len(openset) > 0:
		
		if current == e:
			path = []
			while not came_from[current] == None:
				path.append(came_from[current])
				current = came_from[current]
			path.append(None)
			return path
		closedset.append(current)
		
		openset.remove(current)
		
		for test in current.next.values():
			if not ( "WALL" in test.FLAGS or test in closedset):
					
				test_g_score = g_score[current] + getdircost( test.pos , current.pos )
				cond = False
				
				try:
					if not test in openset or test_g_score < g_score[test]:
						cond = True
				except ValueError:
					if not test in openset:
						cond = True
						
				if cond == True:		
					came_from[test] = current
					g_score[test] = test_g_score
					f_score[test] = g_score[test] +  get_h_score(s.pos, test.pos)
					openset.append(test)
		current = min (openset, key=lambda candidate: f_score[candidate])

			
def A_star_estimate(e,s):
	openset = [] # The set of tentative nodes to be evaluated, initially containing the start node
	closedset = []
	
	openset.append(s)
	current = s	
	
	g_score = {}
	came_from = {}
	came_from[s] = None
	g_score[s] = 0
	
	while len(openset) > 0:
		
		if current == e:
			return came_from[current]
			#return came_from.values
		closedset.append(current)
		
		openset.remove(current)	
		
		for test in current.connections:
			if not (test in closedset):
				
				test_g_score = g_score[current] + 1
				cond = False
				
				try:
					if not test in openset or test_g_score < g_score[test]:
						cond = True
				except ValueError:
					if not test in openset:
						cond = True
						
				if cond == True:		
					came_from[test] = current
					g_score[test] = test_g_score
					openset.append(test)	
					
					
		current = min (openset, key=lambda candidate: g_score[candidate])
					
					
def getdircost(loc1,loc2):    
	if loc1[0] - loc2[0] != 0 and loc1[1] - loc2[1] != 0:
		return 1.4 # diagnal movement
	else:
		return 1.0 # horizontal/vertical movement 
		
def get_h_score(end,current):
	hscore = (abs(end[0]-current[0])+abs(end[1]-current[1]))
	return hscore
	