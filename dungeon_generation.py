import pygame
import random
import math

import generation_related_functions
import terrain_generation
import dungeon_types

needed_parameters = {dungeon_types.simple_room_complex: [[generation_related_functions.random_area_name,"room_complex"], [generation_related_functions.create_rooms,[8,14]]],
					 dungeon_types.celluar_cave:		[[generation_related_functions.random_area_name,"cave"], [generation_related_functions.random_size,[30,60,30,60]]]}

def create_dungeon( AMOUNT_LEVELS = 1,
					AREAS_PER_LEVEL = [10],
					#POSSIBLE_AREA_TYPES = [dungeon_types.simple_room_complex]):
					POSSIBLE_AREA_TYPES = [ [dungeon_types.celluar_cave,.45],
											[dungeon_types.simple_room_complex,.55]]):
		LEVELS = []
		while len(LEVELS) < AMOUNT_LEVELS:
			AREAS =  []
			while len(AREAS) < AREAS_PER_LEVEL[len(LEVELS)]:
				type = None
				while type == None:
					candidate = random.sample(POSSIBLE_AREA_TYPES, 1)[0]
					if random.uniform(0,1) < candidate[-1]:
						type = candidate[0]
				wrapper_list = needed_parameters[type][:]
				parameters = []
				for p in wrapper_list:
					f = p[0]
					para = p[1:]
					res = f(*para)
					parameters.append(res)
					
				new_area = type(*parameters)
				AREAS.append(new_area)
			blueprint = arrange_areas(AREAS)
			LEVELS.append(blueprint)

		print "donen"
		return blueprint
		
def arrange_areas(areas): #creates blueprint of level containing areas
	pairs = {"UP": "DOWN",
			 "DOWN": "UP",
			 "LEFT": "RIGHT",
			 "RIGHT": "LEFT"}


		

	connected = {}
	rects = []
	start = areas[0]
	areas.remove(start)
	start.rect.topleft = (0,0)
	rects.append(start.rect)
	connected[start] = 0
	

		
	while(len(areas) > 0):				
		#print len(areas),"left.",
		new_r = random.sample(areas, 1)[0]
		
		#old_r = random.sample(connected.keys(), 1)[0]
		
		#test
		size = len(connected.keys())/3
		if size == 0:
			size = 1
		base_sample = random.sample(connected.keys(), size)
		old_r =  max (base_sample, key=lambda test: connected[test])

		
		index = 0
		side2 = random.sample(new_r.connection_edges, 1)[0]
		side1 = random.sample(old_r.connection_edges, 1)[0]
		#for side2 in new_r.connection_edges:
		#	for side1 in old_r.connection_edges:
		if pairs[side1[0]] == side2[0] and side1[-1] and side2[-1] and not new_r in connected.keys():

		#	test_surf = pygame.Surface((400,400))
		#	test_surf.fill((0,0,0))
			#print side1[1],side1[2],side1[3],
			#print side2[1],side2[2],side2[3]
			c1 = side1[3]
			c2 = side2[3]
						
			#if side1[0] == "UP" or side1[0] == "DOWN":
			x = old_r.rect.left + c1[0] - c2[0]
			y = old_r.rect.top  + c1[1] - c2[1]
			#else:
			#	x = old_r.rect.left + c2[0] - c1[0]
			#	y = old_r.rect.top  + c2[1] - c1[1]			
			
			new_r.rect.topleft = (x,y)

			fail = False
			#testing
			#t_rect1 = new_r.rect.inflate(-8, -8)
			t_rect1 = new_r.rect.inflate(-16*2, -16*2)
			for area in connected.keys():
				#t_rect2 = area.rect.inflate(-8, -8)
				t_rect2 = area.rect.inflate(-16*2, -16*2)
				if t_rect1.colliderect(t_rect2):
					fail = True
					break
			if not fail:
				side1[-1] = False
				side2[-1] = False
				connected[new_r] = 1
				connected[old_r]+= 1
				rects.append(new_r.rect)
				areas.remove(new_r)
				new_r.connections[old_r] = [side1, True]
				old_r.connections[new_r] = [side2, True]

		#	pygame.draw.rect(test_surf, (100,100,c1[0]), old_r, 0)
		#	pygame.draw.rect(test_surf, (100,100,c2[0]), new_r, 0)
		#	pygame.image.save(test_surf,"result___"+str(index)+".png")
		#	index += 1

	D = rects[0].unionall(rects)
	n_xo = 0 
	n_yo = 0 
	if D.x < 0:
		n_xo = math.fabs(D.x)
	if D.y < 0:
		n_yo = math.fabs(D.y)	
	for room in connected.keys():
		room.rect.move_ip(n_xo,n_yo)
		

	s_rect = rects[0].unionall(rects)
	MAXX = 	s_rect.width
	MAXY = 	s_rect.height

	s_surf = pygame.Surface((MAXX,MAXY))
	
	s_surf.fill((0,0,0))
	
#	index = 0
#	test_surf = s_surf.copy()
	for room in connected.keys():
		room.surf.set_colorkey((0,0,0))
		s_surf.blit(room.surf,room.rect)
		room.surf.set_colorkey()

		#		test_surf.blit(room.surf,room.rect)
#		pygame.image.save(test_surf,"result_"+str(index)+".png")
#		test_surf.fill((0,0,0))
#		index +=1
	for room in connected.keys():
		for key in room.connections.keys():
			#print room.connections[key][-1]
			if room.connections[key][-1]:
				side = room.connections[key][0]
				#c_p = map(sum, zip(room.rect.topleft, side[3]))
				c_p = map(sum, zip(key.rect.topleft, side[3]))
				#print room.rect,c_p
				#print room.rect.bottomleft,c_p
				line = [side[1][0]-side[2][0],side[1][1]-side[2][1]]
				path = [-line[1]/2,line[0]/2]
				s = [c_p[0]-path[0], c_p[1]-path[1]]
				e = [c_p[0]+path[0], c_p[1]+path[1]]
				pygame.draw.line(s_surf,(255,255,255), s, e, 2)
				#debug
				#pygame.draw.circle(s_surf, (200,100,0), side[3], 5)
				#pygame.draw.line(s_surf,(0,0,255), s, room.rect.center, 1)
				#pygame.draw.line(s_surf,(0,0,255), e, key.rect.center, 1)
				
				room.connections[key][-1] = False
				key.connections[room][-1] = False
				
	pygame.image.save(s_surf,"result_.png")
	return s_surf
	#for room in base_areas.keys():
	#	room.OPEN_COLOUR = (55,155,25)
	#set_areas[0].OPEN_COLOUR = (255,155,55)
	#for room in set_areas:
	#	x = len(room.connections)*25
	#	room.OPEN_COLOUR = (x,0,x)

		
			
			
			
def create_dungeon_random( AMOUT_AREAS = 10,
					MIN_W = 40,
					MIN_H = 40,
					MAX_W = 140,
					MAX_H = 140,
					RANDOM_VARIANCE = True):
	r_count = 0
	x,y = 0,0				
	areas = []
	cave_rects = []
	cata_rects = []
	while len(areas) < AMOUT_AREAS:
		if random.randint(0,1) == 0:
			w = random.randint(MIN_W,MAX_W)
		else:
			w = random.randint(-MAX_W,-MIN_W)
		if random.randint(0,1) == 0:
			h = random.randint(MIN_H,MAX_H)
		else:
			h = random.randint(-MAX_H,-MIN_H)
		r = pygame.Rect(x,y,w,h)
		r.normalize()
		fail = False
		for rect in areas:
			if r.colliderect(rect):
				fail = True
				
		if not fail:
			areas.append(r)
		else:
			r_count += 1
			base_r = random.sample(areas, 1)[0]
			#ny = random.randint(0,base_r.height)
			#nx = random.randint(0, base_r.width)
			if random.randint(0,1) == 0:
				if random.randint(0,1) == 0:
					nx = base_r.width
				else:
					nx = 0
				ny = random.randint(int(base_r.height*.3), int(base_r.height*.7))
				#ny = random.randint(int(base_r.height*.5)-1, int(base_r.height*.5)+1)
			else:
				if random.randint(0,1) == 0:
					ny = base_r.height
				else:
					ny = 0
				nx = random.randint(int(base_r.width*.3), int(base_r.width*.7))
				#nx = random.randint(int(base_r.width*.5)-1, int(base_r.width*.5)+1)
			x = base_r.left+nx
			y = base_r.top+ny
		
		
	for area in areas:
		if random.randint(0,1) == 0:
			#cave_rects.append(area)
			cave_rects.append(area)
		else:
			cata_rects.append(area)
	D = areas[0].unionall(areas)
	n_xo = 0 
	n_yo = 0 
	if D.x < 0:
		n_xo = math.fabs(D.x)
	if D.x < 0:
		n_yo = math.fabs(D.y)
	for area in areas:
		area.move_ip(n_xo,n_yo)
		print area
	D = areas[0].unionall(areas)	
	print "in_total", D
	print "rejected",r_count,"areas while generating"
	MAXX = D.width
	MAXY = D.height
	return terrain_generation.create_level (DIMENSIONS = (MAXX,MAXY), cata_rects=cata_rects, cave_rects=cave_rects)


def create_dungeon_division( MAXX = 200,
					MAXY = 200,
					AMOUT_AREAS = 10,
					RANDOM_VARIANCE = True,):
					
	base = pygame.Rect(0,0,200,200)
	areas = [base]
	cave_rects = []
	cata_rects = []
	while len(areas) < AMOUT_AREAS:
		test = areas[0]
		if test.width > test.height:
			if RANDOM_VARIANCE:
				limit = random.randint(0, test.width / 5)
				variance = random.randint( -limit , limit )
			w1 = test.width/2 + variance
			w2 = test.width - w1
			h1 = h2 = test.height
			x2, y2 = test.left + w1 , test.top
		else:
			if RANDOM_VARIANCE:
				limit = random.randint(0, test.height / 5)
				variance = random.randint( -limit , limit )
			h1 = test.height/2  + variance
			h2 = test.height - h1
			w1 = w2 = test.width
			x2 , y2 = test.left , test.top + h1
			
		new1 = pygame.Rect(test.left, test.top, w1 , h1)
		new2 = pygame.Rect(x2, y2, w2 , h2)
		areas.remove(test)					
		areas.append(new1)
		areas.append(new2)
	for area in areas:
		if random.randint(0,1) == 0:
			cave_rects.append(area)
		else:
			cata_rects.append(area)
	return terrain_generation.create_level (DIMENSIONS = (MAXX,MAXY), cata_rects=cata_rects, cave_rects=cave_rects)



	