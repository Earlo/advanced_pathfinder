
import pygame
import random

class tile(object):
	def __init__(self, size, pos, stage, FLAGS = [] , width = 1, colour = (0,0,0) ):
		
		self.content = []
		
		self.node = None #these are used to generalize pathfinding to make it faster
		self.dynamic = False
		self.occupier = None
		self.FLAGS = ["UNEXPLORED","TILE"]
		
		for flag in FLAGS:
			self.FLAGS.append(flag)

		#self.colour = (0,0,0)
		#shade = random.randint(0,30)
		#self.colour = (shade,shade,shade)
		self.colour = colour
		self.width = width
		self.rect = pygame.Rect(pos[0]*size,pos[1]*size,size,size)
		self.size = size
		self.pos = pos
		self.stage = stage
		
		self.lit = False
		self.ligth_sources = []
		
	def blit(self):
		from game import PROGRAM
		if PROGRAM.game_camera.contains(self.rect):
			srect = (self.rect.move(-PROGRAM.game_camera.left,-PROGRAM.game_camera.top)) #rect for the screen
			PROGRAM.MainWindow.blit(self.stage.surf_bgr, srect.topleft, area = self.rect)
			PROGRAM.updates.append(srect)	
	
	def raw_blit(self):
		from game import PROGRAM
		if PROGRAM.game_camera.contains(self.rect):
			srect = (self.rect.move(-PROGRAM.game_camera.left,-PROGRAM.game_camera.top)) #rect for the screen
			PROGRAM.MainWindow.blit(self.stage.surf_bgr, srect.topleft, area = self.rect)

	def shade(self):
		from game import PROGRAM
		self.lit = False
		#debug
		pygame.draw.rect(self.stage.surf_dar,(55,55,55),self.rect)
		shaded_colour = (self.colour[0]/2,self.colour[1]/2,self.colour[2]/2)
		pygame.draw.rect(self.stage.surf_dar,shaded_colour,self.rect,self.width)
		#test 1
		pygame.draw.rect(PROGRAM.stage.surf_obj,(255,255,255),self.rect,0)#remove anything from obj layer on the tile		

		if PROGRAM.game_camera.contains(self.rect):
			srect = (self.rect.move(-PROGRAM.game_camera.left,-PROGRAM.game_camera.top)) #rect for the screen
			if not self.occupier == None:
				if self.occupier.shape == "CIRC":
					item_colour = (self.occupier.colour[0]/2,self.occupier.colour[1]/2,self.occupier.colour[2]/2)
					pygame.draw.circle(self.stage.surf_dar,item_colour,self.rect.center,self.occupier.size)
				
			PROGRAM.MainWindow.blit(self.stage.surf_dar, srect.topleft, area = self.rect)
			PROGRAM.updates.append(srect)
				
	def un_shade(self):
		from game import PROGRAM
		if "UNEXPLORED" in self.FLAGS:
			self.FLAGS.remove("UNEXPLORED")
		self.lit = True
		PROGRAM.draw[0:0] = self.content
		
		if not self.occupier == None:
			PROGRAM.draw.append(self.occupier)
		pygame.draw.rect(self.stage.surf_dar,(255,255,255),self.rect)
		self.blit()

		
	#def draw(self, update = True):
	def draw(self,update = True):
		if self.lit:
			from game import PROGRAM
			pygame.draw.rect(PROGRAM.stage.surf_obj,(255,255,255),self.rect,0)#remove anything from obj layer on the tile		
			if not self.occupier == None:
				self.draw_content(self.occupier)
			elif not self.content == []:
				self.draw_content(self.content[-1])
			else:
				pygame.draw.rect(PROGRAM.stage.surf_bgr,self.colour,self.rect,self.width)		
				self.blit()	
				
	def draw_content(self,item):
		if self.lit:
			from game import PROGRAM
			srect = (self.rect.move(-PROGRAM.game_camera.left,-PROGRAM.game_camera.top)) #rect for the screen
			if item.shape == "CIRC":
				pygame.draw.circle(self.stage.surf_obj,item.colour,self.rect.center,item.size)
			if PROGRAM.game_camera.contains(self.rect):	
				PROGRAM.MainWindow.blit(self.stage.surf_bgr, srect.topleft, area = self.rect)
				PROGRAM.MainWindow.blit(self.stage.surf_obj, srect.topleft, area = self.rect)
				PROGRAM.updates.append(srect)

	
	def connections(self): #find adjatant tiles, Ugly and somewhat pointless, but, will do.
		#print self.pos
		stage = self.stage
		self.next = {}

		if self.pos[0] > 0 and self.pos[0] < stage.dimensions[0]-1 and self.pos[1] > 0 and self.pos[1] < stage.dimensions[1]-1:
			self.next[(-1,0)] =  stage.tiles[self.pos[0]-1][self.pos[1]]	#left
			self.next[(0,-1)] = stage.tiles[self.pos[0]][self.pos[1]-1] 	#up
			self.next[(1,0)] = stage.tiles[self.pos[0]+1][self.pos[1]] 		#rigth	
			self.next[(0,1)] = stage.tiles[self.pos[0]][self.pos[1]+1] 		#down
			self.next[(-1,-1)] = stage.tiles[self.pos[0]-1][self.pos[1]-1]	#upleft
			self.next[(1,-1)] = stage.tiles[self.pos[0]+1][self.pos[1]-1]	#uprigth		# 4 1 5																									
			self.next[(1,1)] = stage.tiles[self.pos[0]+1][self.pos[1]+1]	#downrigth		# 0 x 2
			self.next[(-1,1)] = stage.tiles[self.pos[0]-1][self.pos[1]+1]	#downleft		# 7 3 6
		else:			
			if self.pos[0] == 0: # 4 0 7 side
				self.next[(1,0)] = stage.tiles[self.pos[0]+1][self.pos[1]]		#rigth	
				if not self.pos[1] == 0:
					self.next[(0,-1)] = stage.tiles[self.pos[0]][self.pos[1]-1] #up
					self.next[(1,-1)] = stage.tiles[self.pos[0]+1][self.pos[1]-1]	#uprigth		
				if not self.pos[1] == stage.dimensions[1]-1:
					self.next[(1,1)] = stage.tiles[self.pos[0]+1][self.pos[1]+1]	#downrigth
					self.next[(0,1)] = stage.tiles[self.pos[0]][self.pos[1]+1] 		#down

					
			elif self.pos[0] == stage.dimensions[0]-1: # 5 2 6 side
				self.next[(-1,0)] =  stage.tiles[self.pos[0]-1][self.pos[1]]	#left
				if not self.pos[1] == 0:
					self.next[(0,-1)] = stage.tiles[self.pos[0]][self.pos[1]-1] 	#up
					self.next[(-1,-1)] = stage.tiles[self.pos[0]-1][self.pos[1]-1]	#upleft
				if not self.pos[1] == stage.dimensions[1]-1:
					self.next[(0,1)] = stage.tiles[self.pos[0]][self.pos[1]+1] 		#down
					self.next[(-1,1)] = stage.tiles[self.pos[0]-1][self.pos[1]+1]	#downleft		
					
					
			if self.pos[1] == 0: # 7 3 6 side
				self.next[(0,1)] = stage.tiles[self.pos[0]][self.pos[1]+1] 		#down
				if not self.pos[0] == 0 and not self.pos[0] == stage.dimensions[0]-1:
					self.next[(1,0)] = stage.tiles[self.pos[0]+1][self.pos[1]] 		#rigth	
					self.next[(1,1)] = stage.tiles[self.pos[0]+1][self.pos[1]+1]	#downrigth	
					self.next[(-1,0)] =  stage.tiles[self.pos[0]-1][self.pos[1]]	#left
					self.next[(-1,1)] = stage.tiles[self.pos[0]-1][self.pos[1]+1]	#downleft	

					
			elif self.pos[1] == stage.dimensions[1]-1: # 4 1 5 side
				self.next[(0,-1)] = stage.tiles[self.pos[0]][self.pos[1]-1] 	#up
				if not self.pos[0] == 0 and not self.pos[0] == stage.dimensions[0]-1: 
					self.next[(-1,0)] =  stage.tiles[self.pos[0]-1][self.pos[1]]	#left
					self.next[(-1,-1)] = stage.tiles[self.pos[0]-1][self.pos[1]-1]	#upleft
					self.next[(1,0)] = stage.tiles[self.pos[0]+1][self.pos[1]] 		#rigth	
					self.next[(1,-1)] = stage.tiles[self.pos[0]+1][self.pos[1]-1]	#uprigth	
			




class node(object): #bunch of tiles are heaped together in here to help AI and pathfinding
	def __init__(self, colour = (0,0,0)):
		self.colour = colour #mostly for debugging
		self.tiles = []
		self.edges = [] #border tiles go in here
		self.connections = []
		self.objects = []
		
	