import pygame
	
class item1(object):
	def __init__(self, tile ):
		self.name = "Item #1"
		self.shape = "CIRC"
		self.size = 4
		self.colour = (50,230,10)
		self.rect = pygame.Rect(0,0,8,8)
		
		self.tile = tile
		self.tile.content.append(self)
		self.FLAGS = ["ITEM"]
		
	def setup(self):
		self.draw()

	def ligth(self):
		vision.player_sigth(self)
		
	def draw(self):
		self.tile.draw()	
		
class item2(object):
	def __init__(self, tile ):
		self.name = "Item #2"
		self.shape = "CIRC"
		self.size = 4
		self.colour = (50,10,230)
		self.rect = pygame.Rect(0,0,8,8)
		
		self.tile = tile
		self.tile.content.append(self)
		self.FLAGS = ["ITEM"]
		
	def setup(self):
		self.draw()

	def ligth(self):
		vision.player_sigth(self)
		
	def draw(self):
		self.tile.draw()	
	