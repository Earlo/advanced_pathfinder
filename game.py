import pygame
import os
from pygame.locals import *


import room,screen,menu,unit,behaviour,char_memories,items,inventory




class game():
	
	def __init__(self):
		
		self.FPS = 30 # 60 frames per second
		
		#self.FPS = 6 # 60 frames per second
		
		self.clock = pygame.time.Clock()
		SWIDTH =  1000
		SHEIGTH = 600	
		self.MainWindow = pygame.display.set_mode((SWIDTH, SHEIGTH))
		self.selected_stage = "Stage0"
		
		self.mouse = [pygame.mouse.get_pos() , False ,  [0,0] , None ]
		self.keys = pygame.key.get_pressed()

		self.game_keymap = {}
		
		self.surf_GUI = pygame.Surface((SWIDTH, SHEIGTH))
		self.updates = []
		self.erase = []
		self.draw = []
		
		#path = os.path.join("recourses","cursor.bmp")
		
		self.funktion = None
		self.GUI = []
		self.done = False
		self.debug_message = ""
	
	def m_loop(self):
		while not self.done:
			#self.keys = [self.keys,pygame.key.get_pressed()] , key=lambda candidate: vector.simple_distance(candidate.pos,char.tile.pos))
			self.keys = pygame.key.get_pressed()
			self.mouse[0] = pygame.mouse.get_pos()
			self.mouse[1] = False
			for event in pygame.event.get(): # User did something
				if event.type == pygame.QUIT: # Closed from X
					self.done = True # Stop the Loop
				if event.type == pygame.MOUSEBUTTONUP:
					self.mouse[1] = [True]
					for object in self.GUI:#check if any in game buttons are pressed
						if object.pressed(self.mouse[0]):
							object.act()
							
					
				if event.type == pygame.KEYDOWN:
					try:
						self.run_command(self.game_keymap[event.key])
					except KeyError:
						pass
						#key pressed was mapped for nothing
			self.funktion()
						
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Start menu setup and loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~						
	def sstart_menu(self):
		self.GUI = []
		buttons = []
		
		buttons.append(menu.button(pygame.Rect(400,100,200,100),"Start Game",[self.sgame]))
		buttons.append(menu.button(pygame.Rect(400,300,200,100),"Select Stage",[self.sstage_menu]))
		menu_bar = menu.menu_box(pygame.Rect(300,50,400,400),buttons,(25,50,100))
		self.GUI.append(menu_bar)
		
		self.MainWindow.fill((100,100,100))
		self.refresh_GUI()
		pygame.display.flip()
		self.funktion = self.start_menu
		
	def start_menu(self):
		pygame.display.update(self.updates)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Stage select menu setup and loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~			
	def sstage_menu(self):
		self.GUI = []
		self.GUI.append(menu.button(pygame.Rect(self.MainWindow.get_size()[0]-180,self.MainWindow.get_size()[1]-100,160,80),"Main menu",[self.sstart_menu]))
		
		x = 0
		for file in os.listdir('stages'):
			name = file.split(".")[0]
			self.GUI.append(menu.button(pygame.Rect(10+100*x,10,80,60),name,[self.change_stage, name, "ONETIME"]))
			x += 1
			
			
		self.MainWindow.fill((100,100,100))
		
		self.refresh_GUI()
		
		pygame.display.flip()
		self.funktion = self.stage_menu		
		
	def stage_menu(self):
		pass
		
	def change_stage(self, new):
		new = new[0]
		if not self.selected_stage == new:
			old = self.selected_stage
			self.selected_stage = new		
			print "default stage changed to",new,"from",old,"\n" 
		else:
			print "default stage already is",new,"\n" 	
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Pausemenu setup and loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	
	def spmenu (self):
		self.game_keymap = {}
		self.game_keymap[K_ESCAPE] = [[self.rgame]]

		self.GUI = []
		
		buttons = []
		#buttons.append(menu.button(pygame.Rect(self.MainWindow.get_size()[0]-180,self.MainWindow.get_size()[1]-100,160,80),"Main menu",[self.sstart_menu]))
		buttons.append(menu.button(pygame.Rect(400,100,200,80),"Main menu",[self.sstart_menu]))
		
		buttons.append(menu.button(pygame.Rect(678, 52, 20,20),"X",[self.rgame]))

		pause_menu = menu.menu_box(pygame.Rect(300,50,400,400),buttons,(25,50,100))		
		
		self.GUI.append(pause_menu)
		
		self.refresh_GUI()
		self.funktion = self.pmenu
		
	def pmenu (self):
		pygame.display.update(self.updates)
		
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Game setup and loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	
	def sgame(self):
		#setting up important variables
		self.GUI = []
		self.draw =  []
		self.units = []
		self.game_keymap = {}
		self.turncount = 0

		#loading stage and setting it up
		self.stage = room.level(self.selected_stage)
		self.stage.connections()
		self.stage.create_rooms()
		self.game_camera = screen.Camera(self.stage.LevelRect, self.MainWindow.get_size()[0]-200, self.MainWindow.get_size()[1])
		
		#player
		self.PLAYER = None
		for row in self.stage.tiles:
			for tile in row:
				if not 'WALL' in tile.FLAGS and self.PLAYER == None:
					self.PLAYER =  unit.unit(tile, AI = False)
					break
		print "dodny"
		self.units.append(self.PLAYER)		
		
		#setting up GUI
		pause_button = menu.button(pygame.Rect(self.MainWindow.get_size()[0]-180, 50, 160,50),"Pause Menu",[self.spmenu])
		
		sidebar_rect = pygame.Rect(self.MainWindow.get_size()[0]-200,0,200,self.MainWindow.get_size()[1])
		sidebar_buttons = []
		sidebar_buttons.append(self.PLAYER.inventory)
		sidebar_buttons.append(self.PLAYER.surrounding_loot)
		sidebar_buttons.append(pause_button)
		sidebar = menu.menu_box(sidebar_rect,sidebar_buttons,(50,50,50))
		PROGRAM.GUI.append(sidebar)
		
		#setting keys
		self.game_keymap[K_ESCAPE] = [[self.spmenu]]

		#setting up NPC's debug mostly
		for x in range(0,0):
			#debug
			enemy = unit.unit(self.stage.tiles[5+x*10][5+x*10])
			#enemy.behaviour = behaviour.pathfind_tester
			enemy.behaviour = [behaviour.wanderer]
			#enemy.target = self.PLAYER
			enemy.speed = 1
			enemy.colour  = (255,155,55)
			
			self.units.append(enemy)
			
		#enemy = unit.unit(self.stage.tiles[2][1])
		#enemy.behaviour = behaviour.pathfind_tester
		#enemy.behaviour = [behaviour.pathfind_tester]
		#enemy.behaviour = [behaviour.stalker]
		#enemy.memory[self.PLAYER] = char_memories.unit_info(self.PLAYER)
		#enemy.memory[self.PLAYER].tile = [self.PLAYER.tile,0]
		#enemy.target = enemy.memory[self.PLAYER]
		#enemy.speed = 2
		#enemy.colour  = (155,55,55)
		
		#self.units.append(enemy)	
		
		#setting up items in ground
	#	for x in range(0,50):
	#		i = items.item1(self.stage.tiles[20][20])
	#		self.stage.items.append(i)
	#	for x in range(0,50):
	#		i = items.item2(self.stage.tiles[21][20])
	#		self.stage.items.append(i)
		
		#drawing stuff
		self.stage.draw()
		self.refresh_GUI()
		for item in self.stage.items:
			item.setup()
				
		print "done setting up"
		self.funktion = self.game
		self.PLAYER.FOW()
		pygame.display.flip()			

	def rgame(self): #return to game
		self.game_keymap = {}
		self.GUI = []
		
		#setting up GUI
		#setting up GUI
		pause_button = menu.button(pygame.Rect(self.MainWindow.get_size()[0]-180, 50, 160,50),"Pause Menu",[self.spmenu])
		
		sidebar_rect = pygame.Rect(self.MainWindow.get_size()[0]-200,0,200,self.MainWindow.get_size()[1])
		sidebar_buttons = []
		sidebar_buttons.append(self.PLAYER.inventory)
		sidebar_buttons.append(self.PLAYER.surrounding_loot)
		sidebar_buttons.append(pause_button)
		sidebar = menu.menu_box(sidebar_rect,sidebar_buttons,(50,50,50))
		PROGRAM.GUI.append(sidebar)
		
		
		#setting keys
		self.game_keymap[K_ESCAPE] = [[self.spmenu]]
		
		self.stage.blit_level(self.game_camera)
		pygame.display.flip()			
		self.funktion = self.game
		
	def game(self):	
		#player controls
		self.mouse[2] =  map(sum, zip( self.mouse[0] , self.game_camera.topleft ))
		if self.mouse[1]: 
			if self.game_camera.collidepoint(self.mouse[2]):#if in game screen
				t = self.mouse[2][0]/self.stage.tile_size , self.mouse[2][1]/self.stage.tile_size
				m_tile =  self.stage.tiles[t[0]][t[1]]
				if not "WALL" in m_tile.FLAGS and not "UNEXPLORED" in m_tile.FLAGS:
					if self.mouse[3] == None:
						self.PLAYER.target = self.stage.tiles[t[0]][t[1]]						
					else:
						if m_tile in self.PLAYER.tile.next.values() or m_tile == self.PLAYER.tile:
							inventory.transfer_item_to(m_tile , self.mouse[3])
							m_tile.draw()
							self.mouse[3] = None
							self.PLAYER.check_loot()
		x = 0
		max = len(self.units)
		start = pygame.time.get_ticks()
		current =  pygame.time.get_ticks() - start
		
		while (current < 16 and max > x):
	#	while (max > x):
	#	while (current < 16):
			#if max > x:
			turn_end = self.units[0].act()
			if turn_end:
				x += 1
				self.turncount += 1
				self.units.append(self.units[0])
				self.units.pop(0)
			current =  pygame.time.get_ticks() - start

		f = self.game_camera.update(self.PLAYER.tile.rect.topleft)
		for obj in self.draw:
			obj.draw()

		self.draw =  []

		if not f:
			pygame.display.update(self.updates)
		else:
			#print "kyyly"
			#for item in self.stage.items:
			#	item.draw()
			pygame.display.flip()	
		#pygame.display.flip()			
		self.updates = []
		self.clock.tick(self.FPS)
		#pygame.display.set_caption("FPS: %i" % self.clock.get_fps())
		
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ General Game Functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	def run_command(self, commands):
		for c in commands:
			command = c[:]
			if command[0] == "ONETIME":
				command.pop(0)
				f = command[0]
				command.pop(0)
				f(*command)
			else:
				self.funktion = command[0]
				
	def refresh_GUI(self):
		for object in self.GUI:
			object.draw()
			
	def add_to_(self,object,list):
		list.append(object)



def start():
	global PROGRAM
	PROGRAM = game()
	PROGRAM.funktion = PROGRAM.sstart_menu
	PROGRAM.m_loop()
	pygame.quit()		