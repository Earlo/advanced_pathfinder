import vision
class unmobile_lamp(object):
	def __init__(self, tile ):
		
		self.shape = "CIRC"
		self.size = 6
		self.colour = (245,230,10)
		
		self.seen_tiles = []
		self.tile = tile
		self.tile.content.append(self)
		self.light_RAD = 20
		self.ligth_accury = 1
		self.FLAGS = ["FURN","UNMB","SOLID","LAMP"]
		
	def setup(self):
		self.ligth()
		self.draw()

	def ligth(self):
		vision.player_sigth(self)
		
	def draw(self):
		self.tile.draw_content(self)	
		
	