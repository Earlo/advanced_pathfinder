
import pygame
import random
import math
import vector

import perlin

def get_area(w,h):
	return w*h

def create_level( 
			DIMENSIONS = (100,100),
			cata_rects = [],
			cave_rects = []):
			
		blueprint = pygame.Surface(DIMENSIONS)
		#blueprint.fill((255,255,255))
		blueprint.fill((0,0,0))
		rect = blueprint.get_rect()
		#cata_rect = pygame.Rect(0,0,100,50)
		#cata_rect1 = pygame.Rect(0,0,100,50)
		#cata_rect2 = pygame.Rect(70,50,30,50)
		#cata_rect3 = pygame.Rect(0,50,30,50)
		#cata_rect4 = pygame.Rect(30,80,40,20)
		#cata_rects.append(cata_rect1)
		#cata_rects.append(cata_rect2)
		#cata_rects.append(cata_rect3)
		#cata_rects.append(cata_rect4)
		
		#cave_rect1 = pygame.Rect(30,50,40,30)
		#cave_rects.append(cave_rect1)
		
		create_simple_catacombs(cata_rects, blueprint)
		create_simple_cave(cave_rects, blueprint)		
		
		
		pygame.image.save(blueprint,"result0.png")
		return blueprint

def create_simple_catacombs(star_area_rects,
							blueprint,
							ROOM_SIZE_MIN = 25, #these do nothing yet s_s
							ROOM_SIZE_AVG = 100,
							ROOM_SIZE_MAX = 225,
							HALLWAY_WIDTH = 2,
							RELATIVE_ROOM_SIZE = 0.75,
							ROOM_AMOUNT_MODIFER = 1,
							WALL_COLOUR = (0,0,0),
							OPEN_COLOUR = (255,255,255),
							DEBUG_COLOURS = False,
							RANDOM_VARIANCE = True):
	total_rooms = []
	for base in star_area_rects:
		#random.randint(0,254)
		if DEBUG_COLOURS:
			OPEN_COLOUR = (random.randint(0,254),random.randint(0,254),random.randint(0,254)),
		areas = [base]
		sections = { }
		key = ""
		variance = 0		
		ROOM_SIZE_AVG = ROOM_SIZE_MIN*4 + math.sqrt(base.width*base.height)**1.2
		AMOUNT_ROOMS = int((base.width*base.height*RELATIVE_ROOM_SIZE*ROOM_AMOUNT_MODIFER)/ROOM_SIZE_AVG)
		print "Room complex of size", base.size,"with",AMOUNT_ROOMS,"rooms"
		
		while (len(areas) < AMOUNT_ROOMS):
			test = areas[0]
			if test.width > test.height:
				if RANDOM_VARIANCE:
					limit = random.randint(0, test.width / 5)
					variance = random.randint( -limit , limit )
				w1 = test.width/2 + variance
				w2 = test.width - w1
				h1 = h2 = test.height
				x2, y2 = test.left + w1 , test.top
			else:
				if RANDOM_VARIANCE:
					limit = random.randint(0, test.height / 5)
					variance = random.randint( -limit , limit )
				h1 = test.height/2  + variance
				h2 = test.height - h1
				w1 = w2 = test.width
				x2 , y2 = test.left , test.top + h1
				
			new1 = pygame.Rect(test.left, test.top, w1 , h1)
			new2 = pygame.Rect(x2, y2, w2 , h2)
			for key, area in sections.iteritems():
				if area == test:
					break
			key1 = key + "0"
			key2 = key + "1"
			areas.remove(test)					
			sections[key1] = new1
			sections[key2] = new2
			areas.append(new1)
			areas.append(new2)
		for area in areas:#create rooms
			room = create_room(area, size = RELATIVE_ROOM_SIZE )
			pygame.draw.rect(blueprint, OPEN_COLOUR , room , 0)
			
		while not sections == {}:#connect rooms
			key1 = sections.keys()[0]
			if key1[-1] == "0":
				key2 = key1[:-1] + "1"
			else:
				key2 = key1[:-1] + "0"
			line = [sections[key1].center,sections[key2].center]
			pygame.draw.line(blueprint, OPEN_COLOUR, line[0], line[1], HALLWAY_WIDTH)
			del sections[key1]
			del sections[key2]
		#total_rooms.extend(areas)
		total_rooms.append(areas)
		
		
	#making areas bit bigger so they overlap with rooms next to them.
	for area in total_rooms:
		for room in area:
			room.inflate_ip((1,1))
	connections = {}
	for section in total_rooms:
		connections[section[0].center] = []
		
	for section in total_rooms:
		tests = total_rooms[:]
		tests.remove(section)

		for test in tests:
			possible_paths = []
			for room1 in section:
				for room2 in test:
					if room1.colliderect(room2):
						if not test in connections[section[0].center]: 
							r1 = create_room(room1,size = RELATIVE_ROOM_SIZE)#.inflate(-1,-1) 
							r2 = create_room(room2,size = RELATIVE_ROOM_SIZE)#.inflate(-1,-1) 

							possible_paths.append([r1,r2])
			if not possible_paths == []:
				line = create_straigth_path(possible_paths)
				if DEBUG_COLOURS:
					#pygame.draw.rect(blueprint,  (240,50,50) , line , 1)
					pygame.draw.line(blueprint, (240,50,50) , line[0], line[1], HALLWAY_WIDTH)
				else:
					pygame.draw.line(blueprint, (255,255,255) , line[0], line[1], HALLWAY_WIDTH)
				connections[section[0].center].append(test)
				connections[test[0].center].append(section)


def create_straigth_path(lines): #will make something smarter later
	candidates = {}
	for line in lines:
		br1 = line[0]
		br2 = line[1]
		rel_pos = vector.check_relative_position(br1,br2)
		if rel_pos: #path along y vector
			b = max((br1.top,br2.top))+4
			t = min((br1.bottom,br2.bottom))-4
			l = max((br1.left,br2.left))
			r = min((br1.right,br2.right))
			h = math.fabs(b-t) 
			w = math.fabs(r-l)
			end_rect = pygame.Rect(l,t,w,h)
			score = end_rect.width 
			p1 = end_rect.midtop
			p2 = end_rect.midbottom
			
		if not rel_pos: #path along x vector
			b = min((br1.bottom,br2.bottom))
			t = max((br1.top,br2.top))
			l = min((br1.right,br2.right))-4
			r = max((br1.left,br2.left))+4
			h = math.fabs(b-t) 
			w = math.fabs(r-l)
			end_rect = pygame.Rect(l,t,w,h)
			score = end_rect.height 
			p1 = end_rect.midleft
			p2 = end_rect.midright		

		
		#candidates[score] = end_rect
		candidates[score] = (p1,p2)
	max_score = max (candidates.keys())
	return candidates[max_score]
	
def create_room(area, size = 0.5):
	w = area.width
	h = area.height
	wr = int(w*size)
	hr = int(h*size)
	
	nx = area.left + (area.width-wr)/2 
	ny = area.top + (area.height-hr)/2
	room = pygame.Rect(nx,ny,wr,hr)
	return room
	
def create_simple_cave( containing_rects,
						blueprint,
						CAVE_UPSCALER = 0.2, #somewhat necessary for cave to connect with other structures
						WALL_COLOUR = (0,0,0),
						OPEN_COLOUR = (255,255,255),
						CENTER_BIAS = 0.5,
						AMP_SCALE =  0.05):
	for containing_rect in containing_rects:
		

		containing_rect.inflate_ip(containing_rect.width*CAVE_UPSCALER,containing_rect.height*CAVE_UPSCALER)	
		containing_rect.clamp_ip(blueprint.get_rect())		
		cenx , ceny = containing_rect.size[0]/2,containing_rect.size[1]/2
		len_d = containing_rect.width
		len_h = containing_rect.height

		test_surface = pygame.Surface((len_d, len_h))
		final_surface = pygame.Surface((len_d, len_h))
		final_surface.fill(WALL_COLOUR)
		
		MAX_BIAS = math.fabs(-(float(len_d/2)**2)/(len_d) +
							 -(float(len_h/2)**2)/(len_h))/2

		print "cave of size",(len_d,len_h),"with max center bias of", MAX_BIAS

		seed = random.randint(10000,100000)
		treshold = -0.02
		#seed = 0
		#print seed
		pn = perlin.SimplexNoise(seed)	
		for x in range(0, len_d):
			for y in range(0, len_h):
				xos = x-cenx
				yos = y-ceny
				
				bias = (-(float(xos)**2)/(len_d + x) +
						-(float(yos)**2)/(len_h + y))/MAX_BIAS + CENTER_BIAS
							
				noise = pn.noise2((x*AMP_SCALE),(y*AMP_SCALE))
				AMP = noise + bias
				if AMP <= treshold: #wall
					test_surface.set_at((x, y), WALL_COLOUR) 
				else: #open
					test_surface.set_at((x, y), OPEN_COLOUR)  
		
		done = False
		openlist = []
		closedlist = []
		cur = [cenx,ceny]
		while openlist == []:
			if test_surface.get_at(cur) == OPEN_COLOUR:
				openlist.append(cur)
			else: #quite lazy will make something sensible later
				cur = [cur[0]+1,cur[1]+1]
				
		while not openlist == []: #delete all other cave piece expect the big main piece
			cur = openlist[0]
			closedlist.append(openlist[0])
		#	print len(closedlist)
			#print len(openlist)
			openlist.pop(0)
			for test in ([cur[0]-1, cur[1]-1], [cur[0], cur[1]-1], [cur[0]+1, cur[1]-1], [cur[0]-1, cur[1]], [cur[0]+1, cur[1]], [cur[0]-1, cur[1]+1], [cur[0], cur[1]+1], [cur[0]+1, cur[1]+1] ):
				if test_surface.get_rect().collidepoint(test):
					if test_surface.get_at(test) == OPEN_COLOUR and (not test in closedlist) and not test in openlist: 
						openlist.append(test)
						final_surface.set_at(test, OPEN_COLOUR) 

		
		o = containing_rect.topleft
		#for p in finallist:
		#	pixel = map(sum, zip(p, o))
		#	blueprint.set_at(pixel, test_surface.get_at(p))  
		final_surface.set_colorkey((0,0,0))
		blueprint.blit(final_surface, o) 


	
	
	
	