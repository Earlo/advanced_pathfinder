import pygame
from pygame.locals import *
import menu 

class  nav_button (object):	#navigation Button
	def __init__(self,rect,text,dir,parent):
		self.rect = rect
		self.dir = dir
		self.parent = parent
		text = text
		font_size = int(self.rect.width/len(text))
		self.font = pygame.font.SysFont("Calibri", font_size)
		self.label = self.font.render(text, 1, (0,0,0))
		
	def draw(self, update = True):
		from game import PROGRAM
		pygame.draw.rect(PROGRAM.surf_GUI, (100,100,100),self.rect,0)
		
		PROGRAM.surf_GUI.blit(self.label, ((self.rect.left+self.rect.width/2) - self.label.get_width()/2, (self.rect.top+self.rect.height/2) - self.label.get_height()/2))
		if update:
			PROGRAM.MainWindow.blit(PROGRAM.surf_GUI, self.rect, self.rect)
			PROGRAM.updates.append(self.rect)
		
	def pressed(self, mouse):
		return self.rect.collidepoint(mouse)
		
	def act(self):
		test = self.parent.y_offset + self.dir 
		if test >= 0 and 0 <= self.parent.dimensions[1] - (self.parent.viewport_size[1]+test):
			self.parent.y_offset = test
			from game import PROGRAM
			PROGRAM.draw.append(self.parent)
		
class item_slot(object):
	def __init__(self, size, pos , parent, width = 1, dynamic = False):
		
		self.content = []		
		self.dynamic = dynamic
		self.parent = parent
		#self.colour = (0,0,0)
		#shade = random.randint(0,30)
		self.FLAGS = ["SLOT"]
		self.colour = (64,64,64)
		self.width = width
		self.rect = pygame.Rect(pos[0],pos[1],size,size)
		self.size = size
		#self.draw()
		
			
	def draw(self,update = False):
		from game import PROGRAM
		r = self.rect.move(0,self.parent.y_offset*(-20))
		if self.parent.contains(r):
			if self.content == []:
				pygame.draw.rect(PROGRAM.surf_GUI,(128,128,128),r,0)		
				pygame.draw.rect(PROGRAM.surf_GUI,self.colour,r,self.width)		
			else:	
				if self.content[0].shape == "CIRC":
					pygame.draw.circle(PROGRAM.surf_GUI,self.content[0].colour,r.center,self.content[0].size)
					pygame.draw.rect(PROGRAM.surf_GUI,self.colour,r,self.width)		
				elif self.contet[0].shape == "RECT":
					pass # ;:^D
			#PROGRAM.MainWindow.blit(PROGRAM.surf_GUI, self.rect, self.rect)
			PROGRAM.MainWindow.blit(PROGRAM.surf_GUI, self.parent, r)
			if update:
				PROGRAM.updates.append(r)
				#you should also check if the slot is in corner or sides, if is, update inventory edges

	def pressed(self, mouse):
		return self.rect.collidepoint(mouse)
			
class Graphical_Inventory(pygame.Rect):	

	def __init__(self, windowWidth, windowHeight, os , size, Dynamic = False ):
		ox , oy = windowWidth-os[0], windowHeight-os[1]
		super(Graphical_Inventory , self).__init__( ox , oy , size[0]*20, size[1]*20)
		size = [size[0]-1,size[1]]
		self.slots = {}
		self.y_offset = 0
		self.buttons = []
		if Dynamic:
			self.additional_lines = []
			r = pygame.Rect(self.right-20,self.top,20,20)
			up_arrow = nav_button(r,"^",-1,self)
			r = pygame.Rect(self.right-20,self.bottom-20,20,20)
			down_arrow = nav_button(r,"v",1,self)
			self.buttons = [up_arrow,down_arrow]
		
		self.viewport_size = size[:]
		self.dimensions = size
		self.original_dimensions = self.dimensions[:]
		for y in range(0,size[1]):
			for x in range(0,size[0]):
				pos = (ox + x*20, oy + y*20,)
				self.slots[(x,y)] = item_slot( 20 , pos, self, dynamic = Dynamic)
				#self.slots.append(item_slot( 20 , pos ))
		self.dynamic = Dynamic
		self.tile_size = 20
	
	def add_line(self):
		self.additional_lines.append([])
		y = self.dimensions[1]
		self.dimensions[1] += 1
		ox,oy = self.topleft
		for x in range(0,self.dimensions[0]):
			pos = (ox + x*20, oy + y*20,)
			s = item_slot( 20 , pos, self, dynamic = self.dynamic)
			self.additional_lines[-1].append((x,y))
			self.slots[(x,y)] = s
		
				
	def draw(self,update = True):
		from game import PROGRAM		
		pygame.draw.rect(PROGRAM.surf_GUI,(128,128,128),self,0)
		for button in self.buttons:
			button.draw()
		for slot in self.slots.values():
			slot.draw()
	#	pygame.draw.rect(PROGRAM.surf_GUI,(0,0,0),self,2 ) #draw outlines
		PROGRAM.MainWindow.blit(PROGRAM.surf_GUI, self , self)
		if update:
			PROGRAM.updates.append(self)
		
	def pressed(self,mouse):
		return self.collidepoint(mouse)
		
	def act(self):
		from game import PROGRAM
		if not self.check_buttons(PROGRAM.mouse[0]):
			
			pos = ( 				(PROGRAM.mouse[0][0] - self.left) / self.tile_size  ,
					self.y_offset + (PROGRAM.mouse[0][1] - self.top) / self.tile_size  )
			try: 
				self.slots[pos]
			except KeyError: #there is no slot here
				return 0
				
			if not self.dynamic: #inventory
				if self.slots[pos].content == []:
					if not PROGRAM.mouse[3] == None:
						transfer_item_to(self.slots[pos] , PROGRAM.mouse[3])
						PROGRAM.mouse[3] = None
				elif PROGRAM.mouse[3] == None:					
					if PROGRAM.keys[K_LSHIFT] or PROGRAM.keys[K_RSHIFT]:
						transfer_item_to(PROGRAM.PLAYER.tile,self.slots[pos].content[0])
					else:
						PROGRAM.mouse[3] = self.slots[pos].content[0]
					self.slots[pos].draw(update = True)

			elif self.dynamic: #surrounding loot
				if not self.slots[pos].content == []:
					if PROGRAM.mouse[3] == None:
						if PROGRAM.keys[K_LSHIFT] or PROGRAM.keys[K_RSHIFT]:
							PROGRAM.PLAYER.inventory.auto_add_item(self.slots[pos].content[0])
						else:
							PROGRAM.mouse[3] = self.slots[pos].content[0]
						self.slots[pos].content = []		
				elif not PROGRAM.mouse[3] == None: 
					self.slots[pos].content =  [PROGRAM.mouse[3]]
					self.slots[pos].draw(update = True)
					if "SLOT" in  PROGRAM.mouse[3].tile.FLAGS: #dropping item
						transfer_item_to(PROGRAM.PLAYER.tile,PROGRAM.mouse[3])
					PROGRAM.mouse[3] = None
				self.slots[pos].draw(update = True)	
			PROGRAM.draw.append(PROGRAM.PLAYER.inventory)
			PROGRAM.PLAYER.check_loot()
			
	def check_buttons(self,mouse):
		for button in self.buttons:
			if button.pressed(mouse):
				button.act()
				return True
		return False
		
	def auto_add_item(self, item):
		done = False
		x = 0 
		y = 0
		while not done:
			if self.slots[(x,y)].content == []:		
				if not self.dynamic:
					item.tile.content.remove(item)
					item.tile.draw()
					item.tile = self.slots[(x,y)]				
				self.slots[(x,y)].content = [item]
				self.slots[(x,y)].draw(update = True)
				done = True
			else:
				x += 1
				if x > self.dimensions[0]-1:
					y += 1
					x = 0 
					if y >  self.dimensions[1]-1:
						if self.dynamic:
							self.add_line()
							#return 0
						else:
							print "Inventory is full!"
							return 0

					
	def empty_slots(self):
		if not self.dynamic:
			print "this should not be happening"
		for line in self.additional_lines:
			for slot in line:
				del self.slots[slot]
		self.additional_lines = []
		from game import PROGRAM		
		for slot in self.slots.values():
			if not slot.content == []:
				slot.content = []
		self.dimensions = self.original_dimensions[:]

	def check_if_contains(self,item):
		for slot in self.slots.values():
			if item in slot.content:
				return True
		return False
	
def transfer_item_to_from(a_slot, o_slot):
	item = o_slot.content[0]
	if a_slot.content == []:
		if not o_slot.dynamic or "TILE" in a_slot.FLAGS:
			o_slot.content.remove(item)
			o_slot.draw(update = True)
			item.tile = a_slot
		a_slot.content.append(item)
		a_slot.draw(update = True)
		
def transfer_item_to(a_slot, item):
	o_slot = item.tile
	if a_slot.content == [] or "TILE" in a_slot.FLAGS:
		if not o_slot.dynamic:
			o_slot.content.remove(item)
			o_slot.draw(update = True)
			item.tile = a_slot
		a_slot.content.append(item)
		a_slot.draw(update = True)
			