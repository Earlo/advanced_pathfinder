import random
import math
import vector

import pygame

import terrain_generation

class simple_room():
	def __init__(	self,
					rect,
					name,
					connections,
					parent,
					WALL_WIDTH = 1):
		self.WALL_WIDTH = WALL_WIDTH
		self.rect = rect
		#self.floor_plan = surf
		self.name = name
		self.connections = connections
		self.parent = parent
		
		self.OPEN_COLOUR = (255,255,255)
		self.WALL_COLOUR = (0,0,0)
 		
	def build(self):
		pygame.draw.rect(self.parent.surf, self.OPEN_COLOUR, self.rect, 0)
		pygame.draw.rect(self.parent.surf, self.WALL_COLOUR, self.rect, self.WALL_WIDTH)
		
	def find_connections(self, rooms):
		new_connections = []
		test_r1 = self.rect.inflate(2,2)
		test_r2 = self.rect.inflate(-self.WALL_WIDTH*2-2, -self.WALL_WIDTH*2-2)
		for room in rooms:
			if test_r1.colliderect(room.rect): #and not room == self:
				test_r3 = room.rect.inflate(-room.WALL_WIDTH*2-2, -room.WALL_WIDTH*2-2)
				if not vector.check_relative_position(test_r2,test_r3) == None:
					self.connections[room] = False
					new_connections.append(room)
					room.connections[self] = False

		return new_connections
	def create_path_connections(self):
		for c_room in self.connections.keys():
			if not self.connections[c_room]:
				line = terrain_generation.create_straigth_path([[self.rect, c_room.rect]])
				pygame.draw.line(self.parent.surf, (255,255,255), line[0], line[1],2)
				self.connections[c_room] = True
				c_room.connections[self] = True
				#debug
				#pygame.draw.line(self.parent.surf, (0,255,0), self.rect.center, c_room.rect.center)
				
				

class simple_room_complex():
	def __init__(	self,
					name,
					rooms,
					MAX_HALLWAY_WIDTH = 2):
		
		self.name = name
		
		self.rooms = []
		rects = []
		r_count = 0
		x,y = 0,0
		self.connections = {}
		base_rooms = {}
		
		r = rooms[0]
		r.rect.topright = (x,y)
		
		r.parent = self
		self.rooms.append(r)
		base_rooms[r] = 0 
		rects.append(r.rect)
		rooms.remove(r)
		base_r = r
		while(len(rooms) > 0):		
			
			r = random.sample(rooms, 1)[0]
			
			if bool(random.getrandbits(1)):
				if bool(random.getrandbits(1)):
					r.rect.topleft = (x,y)
				else:
					r.rect.bottomleft = (x,y)
			else:
				if bool(random.getrandbits(1)):
					r.rect.topright = (x,y)
				else:
					r.rect.bottomright = (x,y)
			fail = False
			t_rect1 = r.rect.inflate(-2, -2)
			
			for room in self.rooms:
				t_rect2 = room.rect.inflate(-8, -8)
				#t_rect2 = room.rect.inflate(0, 0)
				if t_rect1.colliderect(t_rect2):
					fail = True
					break
					
			if not fail:
				connections = r.find_connections(self.rooms)
				if len(connections) > 0:
					r.parent = self
					self.rooms.append(r)
					base_rooms[r] = len(connections)
					for room in connections:
						try:
							base_rooms[room] = len(room.connections)
							if base_rooms[room] > 4:
								del base_rooms[base_r] 
						except KeyError:
							pass
					rects.append(r.rect)
					rooms.remove(r)

			else:
				r_count += 1
				size = len(base_rooms.keys())/3
				if size == 0:
					size = 1
				base_sample = random.sample(base_rooms.keys(), size)
				base_r =  max (base_sample, key=lambda test: base_rooms[test])

				#if bool(random.getrandbits(1)):
				if bool(random.getrandbits(1)):
					nx = base_r.rect.width
				else:
					nx = 0
					#ny = random.randint(int(base_r.rect.height*.5)-1, int(base_r.rect.height*.5)+1)
					#ny = random.randint(0, base_r.rect.height)
			#	else:
				if bool(random.getrandbits(1)):
					ny = base_r.rect.height
				else:
					ny = 0
					#nx = random.randint(int(base_r.rect.width*.5)-1, int(base_r.rect.width*.5)+1)
					#nx = random.randint(0, base_r.rect.width)
				x = base_r.rect.left+nx
				y = base_r.rect.top+ny
			if r_count > 2000:
				r_count = 0
				print "something is taking longer than it should",
				print len(rooms),"rooms left"
			
		D = self.rooms[0].rect.unionall(rects)
		n_xo = 0 
		n_yo = 0 
		if D.x < 0:
			n_xo = math.fabs(D.x)
		if D.y < 0:
			n_yo = math.fabs(D.y)
		for room in self.rooms:
			room.rect.move_ip(n_xo,n_yo)
		self.rect = self.rooms[0].rect.unionall(rects)
		MAXX = 	self.rect.width
		MAXY = 	self.rect.height
		
		self.surf = pygame.Surface((MAXX,MAXY))
		self.surf.fill((0,0,0))
		
		#debug
		#hue = (random.randint(0,255),random.randint(0,255),random.randint(0,255))
		#for room in base_rooms.keys():
		#	room.OPEN_COLOUR = hue
		
		#self.rooms[0].OPEN_COLOUR = (255,155,55)
		#for room in self.rooms:
		#	x = len(room.connections)*25
		#	room.OPEN_COLOUR = (x,0,x)
		
		for room in self.rooms:
			room.build()
		for room in self.rooms:
			room.create_path_connections()
			
					
		check_edges( self )
		for edge in self.connection_edges: 
			edge.append(True) #True, not connected to anything, False, already occupied
			#debug
			#pygame.draw.line(self.surf,(0,0,255), edge[1], edge[2], 1)
		pygame.image.save(self.surf,"result1.png")
			
		#self.save():
		#return terrain_generation.create_level (DIMENSIONS = (MAXX,MAXY), cata_rects=cata_rects, cave_rects=cave_rects)

	#def save(self):
	def check_edges(self):
		x,y = 0,1
		prex,prey = 0,1
		dir = "LEFT"
		maxx = self.surf.get_width()-1
		maxy = self.surf.get_height()-1
		edges = []
		proto_edge = []
		while not (x,y) == (0,0):
			can_connect = False
			for x_os,y_os in ([0,-1],[0,1],[-1,0],[1,0],[0,0]):
				try:
					if not self.surf.get_at((x+x_os, y+y_os)) == wall_colour:
						can_connect = True
						break
				except IndexError:
					pass
					
			if can_connect:
				if len(proto_edge) == 0:
					proto_edge.append(dir)
					proto_edge.append([x,y])
			else:
				if len(proto_edge) == 2:
					proto_edge.append([prex,prey])
					center = [(proto_edge[1][0]+proto_edge[2][0])/2, (proto_edge[1][1]+proto_edge[2][1])/2]
					proto_edge.append(center)
					new_edge = proto_edge[:]
					edges.append(new_edge)
					proto_edge = []
			prex,prey = x,y
			if dir == "LEFT":
				if y == maxy:
					dir = "DOWN"
				else:
					y += 1
			elif dir == "DOWN":
				if x == maxx:
					dir = "RIGHT"
				else:
					x += 1			
			elif dir == "RIGHT":
				if y == 0:
					dir = "UP"
				else:
					y -= 1
			elif dir == "UP":
				if x == 0:
					dir = None
				else:
					x -= 1	
		self.connection_edges = edges
		


class celluar_cave():
	def __init__(	self,	
					name,
					max_size,
					debug_colours = True,
					open_chance = 0.45):
		if debug_colours:
			open_colour = (200,200,200)
			wall_colour = (0,0,0)
		else:
			open_colour = (255,255,255)
			wall_colour = (0,0,0)
		self.name = name
		self.max_size = max_size
		self.connections = {}
		iterations = 5
		
		birthlimit = 4
		deathlimit = 5
		list = []
		for x in range(0,max_size[0]):
			list.append([])
			for y in range(0,max_size[1]):
				list[x].append(random.random() < open_chance) #True = open, False = Wall
				

		
		for time in range(0,iterations):
			new_list = list[:]
			for x in range(0,max_size[0]):
				for y in range(0,max_size[1]):
					n_w = 0 #nearby walls
					for adj in [[-1,-1],[-1,0],[-1,1],[0,-1],[0,1],[1,-1],[1,0],[1,1]]:
						sx,sy = map(sum, zip([x,y], adj))
						try: 
							if not list[sx][sy]:
								n_w += 1
						except IndexError:
							n_w += 1
					if list[x][y]: #is open
						if n_w > deathlimit:
							new_list[x][y] = False
					else: #is wall
						if n_w < birthlimit:
							new_list[x][y] = True
					#else: 
					#	new_list[x][y] = list[x][y]
			list = new_list				
			

		sub_areas = []
		
		for x in range(1,len(list)-1):
			for y in range(1,len(list[0])-1):
				if list[x][y]:
					qualify = True
					for c_list in sub_areas:
						if [x,y] in c_list:
							qualify = False
							break
					if qualify:
						openlist = [[x,y]]
						closedlist = []
						while not openlist == []:
							cur = openlist[0]
							for adj in [[-1,-1],[-1,0],[-1,1],[0,-1],[0,1],[1,-1],[1,0],[1,1]]:	
								sx,sy = map(sum, zip(cur, adj))
								try:
									if list[sx][sy] and [sx,sy] not in openlist and [sx,sy] not in closedlist and sx >= 0 and sy >= 0:
										openlist.append( [sx,sy] )
								except IndexError:
									pass
							openlist.remove(cur)
							closedlist.append(cur)
						sub_areas.append(closedlist)
		max_score = 0
		rand = random.randrange(0,10)
		for c_list in sub_areas:
			if len(c_list) > max_score:
				biggest = [[  min (c_list, key=lambda test: test[0])[0], min (c_list, key=lambda test: test[1])[1]],
							[ max (c_list, key=lambda test: test[0])[0], max (c_list, key=lambda test: test[1])[1]]]
				#max_x = c_list[0][0]
				mx = biggest[1][0] - biggest[0][0]
				my = biggest[1][1] - biggest[0][1]
				bc_list = c_list
				dimensions = (mx,my)
				max_score = len(c_list)
		#debug
#		test_surf = pygame.Surface((max_size))
#		for x in range(0,max_size[0]):
#			for y in range(0,max_size[1]):
#				if list[x][y]:
#					colour = open_colour
#				else:
#					colour = (0,0,0)
#				test_surf.set_at((x, y), colour)
				
		base_surf = pygame.Surface((dimensions))
		base_surf.fill((0,0,0))
		self.surf = base_surf
		for x,y in bc_list:
			if list[x][y]:
				pos = (x - biggest[0][0], y - biggest[0][1])
				base_surf.set_at(pos,open_colour)
				#debug
#				test_surf.set_at((x,y),(255,255,0))
					

		roundness = 3
		n_d = map(sum, zip(dimensions, [roundness*2,roundness*2]))
		rounded_connections = pygame.Surface((n_d))
		
		check_edges( self )
		for nedge in self.connection_edges: 
			#nedge.append(True) #True, not connected to anything, False, already occupied
			edge = [map(sum, zip(nedge[1], [roundness,roundness])),
					map(sum, zip(nedge[2], [roundness,roundness]))]
			needs_rounding = False
			if nedge[0] == "UP" or nedge[0] == "DOWN":
				tl = min (edge, key=lambda test: test[0])[:]
				br = max (edge, key=lambda test: test[0])[:]
				br = [br[0]-tl[0]+1,br[1]-tl[1]+1]
				if br[0] > int(roundness*1.5):
					needs_rounding = True
					tl[1] -= roundness
					br[1] += roundness*2
			else:
				tl = min (edge, key=lambda test: test[1])[:]
				br = max (edge, key=lambda test: test[1])[:]
				br = [br[0]-tl[0]+1,br[1]-tl[1]+1]
				if br[1] > int(roundness*1.5):
					needs_rounding = True
					tl[0] -= roundness
					br[0] += roundness*2
			if needs_rounding:
				pygame.draw.ellipse(rounded_connections, open_colour, (tl,br) , 0)
			#debug
			#pygame.draw.rect(rounded_connections, (255,0,0), (tl,br) , 1)
			#debug
			#pygame.draw.line(rounded_connections,(0,0,255), edge[0], edge[1], 1)
		base_surf.set_colorkey((0,0,0))
		rounded_connections.blit(base_surf, (roundness,roundness), base_surf.get_rect(), special_flags = 0)
		self.surf = rounded_connections
		self.rect = self.surf.get_rect()
		
		
		
		
		
		check_edges( self )
		for nedge in self.connection_edges: 
			nedge.append(True) #True, not connected to anything, False, already occupied
			
		#pygame.image.save(self.surf,"result_"+str(rand)+".png")
		#pygame.image.save(rounded_connections,"resultxy_"+str(rand)+".png")
	

		
def check_edges(room):
	x,y = 0,1
	prex,prey = 0,1
	dir = "LEFT"
	maxx = room.surf.get_width()-1
	maxy = room.surf.get_height()-1
	edges = []
	proto_edge = []
	not_turn = True
	done = False
	#while not (x,y) == (0,0):
	while not done:
		can_connect = False
		for x_os,y_os in ([0,-1],[0,1],[-1,0],[1,0],[0,0]):
			try:
				if not room.surf.get_at((x+x_os, y+y_os)) == (0,0,0):
					can_connect = True
					break
			except IndexError:
				pass
				
		if can_connect and not_turn:
			if len(proto_edge) == 0:
				proto_edge.append(dir)
				proto_edge.append([x,y])
		else:
			if dir == "FINISH":
				done = True
			not_turn = True
			if len(proto_edge) == 2:
				proto_edge.append([prex,prey])
				center = [(proto_edge[1][0]+proto_edge[2][0])/2, (proto_edge[1][1]+proto_edge[2][1])/2]
				proto_edge.append(center)
				new_edge = proto_edge[:]
				edges.append(new_edge)
				proto_edge = []
		prex,prey = x,y
		if dir == "LEFT":
			if y == maxy:
				dir = "DOWN"
				not_turn = False
			else:
				y += 1
		elif dir == "DOWN":
			if x == maxx:
				dir = "RIGHT"
				not_turn = False
			else:
				x += 1			
		elif dir == "RIGHT":
			if y == 0:
				dir = "UP"
				not_turn = False
			else:
				y -= 1
		elif dir == "UP":
			if x == 0:
				dir = "FINISH"
				not_turn = False
			else:
				x -= 1	
	room.connection_edges = edges
