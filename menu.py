import pygame
				
class button (object):	#Menu Button
	def __init__(self,rect,text,funk):
		self.rect = rect
		text = text
		font_size = int(self.rect.width/len(text))
		self.font = pygame.font.SysFont("Calibri", font_size)
		self.label = self.font.render(text, 1, (0,0,0))
		self.funk = funk
		
	def draw(self, update = True):
		from game import PROGRAM
		pygame.draw.rect(PROGRAM.surf_GUI, (200,20,25),self.rect,0)
		
		PROGRAM.surf_GUI.blit(self.label, ((self.rect.left+self.rect.width/2) - self.label.get_width()/2, (self.rect.top+self.rect.height/2) - self.label.get_height()/2))
		if update:
			PROGRAM.MainWindow.blit(PROGRAM.surf_GUI, self.rect, self.rect)
			PROGRAM.updates.append(self.rect)
		
	def pressed(self, mouse):
		return self.rect.collidepoint(mouse)
		
	def act(self):
		from game import PROGRAM
		f = self.funk[:]
		if "ONETIME" in f:
			f.remove("ONETIME")
			one_time_funk = f[0]
			f.pop(0)
			one_time_funk(f)
			f = []
		else:
			PROGRAM.funktion = f[0]

	
class menu_box (object):
	def __init__(self,rect,content,colour):
		self.rect = rect
		self.content = content
		self.colour = colour
		
	def draw(self, update = True):
		from game import PROGRAM
		pygame.draw.rect(PROGRAM.surf_GUI, self.colour ,self.rect,0)
		for x in self.content:
			x.draw(update = False)
		if update:
			PROGRAM.MainWindow.blit(PROGRAM.surf_GUI, self.rect, self.rect)
			PROGRAM.updates.append(self.rect)
	
	def pressed(self, mouse):
		return self.rect.collidepoint(mouse)
		
	def act(self):
		from game import PROGRAM
		for x in self.content:
			if x.pressed(PROGRAM.mouse[0]):
				x.act()
				break
				