import pygame
from pygame.locals import *

import random

import vector
import AI_logic
import vision
import char_memories

keymap = {  K_KP1: (-1 , 1 ),                 
			K_KP2: ( 0 , 1 ),                   
			K_KP3: ( 1 , 1 ),                   
			K_KP4: (-1 , 0 ),                 
			K_KP5: ( 0 , 0 ), 
			K_KP6: ( 1 , 0 ),                
			K_KP7: (-1 ,-1 ),                 
			K_KP8: ( 0 ,-1 ),                 
			K_KP9: ( 1 ,-1 )	}
			
def update_information(char): #Update AI memories of surrounding characters
	from game import PROGRAM
	for node in char.tile.node.connections:
		for obj in node.objects:
			if vector.clear_sigth_test( char.tile , obj.tile ) :
				try:
					char.memory[obj].tile = [obj.tile,PROGRAM.turncount]
				except KeyError:
					if "UNIT" in obj.FLAGS:
						char.memory[obj] = char_memories.unit_info(obj)	
				if "UNIT" in obj.FLAGS:
					for memo in obj.memory.keys():#better interaction stuff will be worked on later
						if memo in char.memory.keys():
							if obj.memory[memo].tile[1] > char.memory[memo].tile[1]:
								char.memory[memo].tile = obj.memory[memo].tile

def check_targets(char):
	if vector.simple_distance(char.tile.pos,char.target.parent.tile.pos) > char.light_RAD:
		if vector.clear_sigth_test( char.tile , char.target.parent.tile ):
			from game import PROGRAM
			char.target.tile =  [char.target.parent.tile , PROGRAM.turncount ]

	
def player(char):
	from game import PROGRAM
	#for debug purposes
#	x = len(char.tile.node.objects) -1
#	for node in char.tile.node.connections:
#		for obj in node.objects:
#			x += 1
#	print "there is",x,"units around you"
	did_something = False
	mov = ( 0 , 0 )

	if not char.target == None and not char.tile == char.target:
		if not vector.straigth_path_test( char.tile , char.target ,char) :
			if char.target_pos[0] == "TILE" and not char.target == char.target_pos[1]:
				char.path = [None]
			elif char.target_pos[0] == "NODE" and not char.target.node == char.target_pos[1]:
				char.path = [None]

			if char.path[0] == None:
				if char.target.node == char.tile.node or char.target.node in char.tile.node.connections:
					#print "target close, looking for exact route"
					target = char.target
					char.target_pos = ["TILE",char.target]
				else:	
					#print "target far away, looking for approximated route"
					next_node = AI_logic.A_star_estimate( char.tile.node , char.target.node )
					targets = char.tile.node.defined_edges[next_node] 		
					target = min (targets, key=lambda candidate: vector.simple_distance(candidate.pos,char.tile.pos))
					char.target_pos = ["NODE",char.target.node]
						
				char.path = AI_logic.A_star_accurate(target , char.tile)
				
	if not char.path[0] == None:
		for key, tile in char.tile.next.iteritems():
			if tile == char.path[0]:
				mov = key
				did_something = True
				char.path.pop(0)
				break	
				
	keys = pygame.key.get_pressed()

	#keys = PROGRAM.keys
	for key, map in keymap.iteritems():
		if keys[key]:
			did_something = True
			char.target = None
			char.path = [None]
			mov = map
			
	if did_something:
		if not mov == ( 0 , 0 ):
			char.surrounding_loot.y_offset = 0
			char.move(mov)
			char.check_loot()
			char.FOW()


	return did_something
		
def idler(char):
	#print "idlers turn"
	did_something = False
	mov = ( 0 , 0 )
	char.move(mov)
	return True
	
def wanderer(char):
	mov = ( 0 , 0 )
	
	update_information(char)						
	
	if char.path == [None]:
		target_node = char.tile.node
		#for x in range(0,random.randint(2,10)):
		target_node = random.sample(target_node.connections,1)[0]
				
		targets = char.tile.node.defined_edges[target_node] 		
		target = min (targets, key=lambda candidate: vector.simple_distance(candidate.pos,char.tile.pos))
					
		char.path = AI_logic.A_star_accurate(target , char.tile)

	for key, tile in char.tile.next.iteritems():
		#print char.path[0]
		if tile == char.path[0]:
			mov = key
			break
	if char.move(mov):
		char.path.pop(0)
	else: #something mobile is blocking the path
		char.path = [None]
		
	return True
	
def agressive_wanderer(char):
	mov = ( 0 , 0 )
	
	update_information(char)	
	check_targets(char)	
						
	if char.path == [None]:
		target_node = char.tile.node
		#for x in range(0,random.randint(2,10)):
		target_node = random.sample(target_node.connections,1)[0]
				
		targets = char.tile.node.defined_edges[target_node] 		
		target = min (targets, key=lambda candidate: vector.simple_distance(candidate.pos,char.tile.pos))
					
		char.path = AI_logic.A_star_accurate(target , char.tile)
		
	if not char.memory[char.target.parent].tile[0] == None: #Know where target is
		char.behaviour.pop(0)
		char.path = None
		return True
		
	for key, tile in char.tile.next.iteritems():
		#print char.path[0]
		if tile == char.path[0]:
			mov = key
			break
	if char.move(mov):
		char.path.pop(0)
	else: #something mobile is blocking the path
		char.path = [None]
		
	return True	


def pathfind_tester(char):
	mov = ( 0 , 0 )
	
	update_information(char)
	
	if not vector.straigth_path_test( char.tile , char.target.tile[0] ,char) :
		if char.target_pos[0] == "TILE" and not char.target.tile[0] == char.target_pos[1]:
			char.path = [None]
		elif char.target_pos[0] == "NODE" and not char.target.tile[0].node == char.target_pos[1]:
			char.path = [None]

		if char.path[0] == None:
			if char.target.tile[0].node == char.tile.node or char.target.tile[0].node in char.tile.node.connections:
				#print "target close, looking for exact route"
				target = char.target.tile[0]
				char.target_pos = ["TILE",char.target.tile[0]]
			else:	
				#print "target far away, looking for approximated route"
				next_node = AI_logic.A_star_estimate( char.tile.node , char.target.tile[0].node )
				targets = char.tile.node.defined_edges[next_node] 		
				target = min (targets, key=lambda candidate: vector.simple_distance(candidate.pos,char.tile.pos))
				char.target_pos = ["NODE",char.target.tile[0].node]
					
			char.path = AI_logic.A_star_accurate(target , char.tile)
	
	if char.path[0] == None: #Couldn't track target
		char.memory[char.target.parent].tile[0] = None
		char.behaviour.insert(0 ,agressive_wanderer)
	
	for key, tile in char.tile.next.iteritems():
		#print char.path[0]
		if tile == char.path[0]:
			mov = key
			break

	if char.move(mov):
		char.path.pop(0)
	

	return True	
	
def stalker(char):
	mov = ( 0 , 0 )
	
	update_information(char)
	check_targets(char)
	
	if not vector.straigth_path_test( char.tile , char.target.tile[0] ,char): #if can't see the target tile calculate the path to the assumed position
		target = char.target.tile[0]

		if char.target_pos[0] == "TILE" and not char.target.tile[0] == char.target_pos[1]:
			char.path = [None]
		elif char.target_pos[0] == "NODE" and not char.target.tile[0].node == char.target_pos[1]:
			char.path = [None]

		if char.path[0] == None:
			if char.target.tile[0].node == char.tile.node or char.target.tile[0].node in char.tile.node.connections:
				#print "target close, looking for exact route"
				target = char.target.tile[0]
				char.target_pos = ["TILE",char.target.tile[0]]
			else:	
				#print "target far away, looking for approximated route"
				next_node = AI_logic.A_star_estimate( char.tile.node , char.target.tile[0].node )
				targets = char.tile.node.defined_edges[next_node] 		
				target = min (targets, key=lambda candidate: vector.simple_distance(candidate.pos,char.tile.pos))
				char.target_pos = ["NODE",char.target.tile[0].node]
			char.path = AI_logic.A_star_accurate(target , char.tile)
			
			for key, tile in char.tile.next.iteritems():
				if tile == char.path[0]:
					mov = key
					break
			
	#elif vector.simple_distance(char.tile.pos , char.target.tile[0].pos ) > 5 or  char.target.tile.[0].occupier == char.target.parent: #if not too close or can't see target
	elif vector.simple_distance(char.tile.pos , char.target.tile[0].pos ) > 5 or not char.target.tile[0].occupier == char.target.parent: #if not too close or can't see target
		for key, tile in char.tile.next.iteritems():
			if tile == char.path[0]:
				mov = key
				break	

	if char.path[0] == None: #Couldn't track target
		char.memory[char.target.parent].tile[0] = None
		char.behaviour.insert(0 , agressive_wanderer)
		return True
					
			
	if char.move(mov):
		char.path.pop(0)
	else:
		char.path = [None]
		
	return True	
