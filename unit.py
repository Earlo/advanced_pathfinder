import pygame
import vision

import inventory
import behaviour
class unit(object):
	def __init__(self,tile, AI = True):
		
		self.FLAGS = ["UNIT"]
		
		tile.occupier = self
		
		self.tile = tile
		self.tile.node.objects.append(self)
		self.colour = (255,0,0)
		
		self.team = None #None means independent
		self.has_turn = False #
		
		self.speed = 10
		self.light_RAD = 10
		#self.light_RAD = 5
		self.ligth_accury = 3
		#self.ligth_accury = 10
		self.AP = 0 #action points
		self.MAX_AP = 150 
		
		
		self.shape = "CIRC"
		self.size = 5
		
		self.movcost = 25
		
		self.memory = {}

	#	self.Semantic_Memory = {}
	#	self.Episodic_Memory = {}
		
		self.target = None
		self.path = [None]
		self.target_pos = [None,None]	
		
		if not AI == True:
			from game import PROGRAM
			
			self.behaviour = [behaviour.player]

			self.inventory = inventory.Graphical_Inventory( PROGRAM.MainWindow.get_size()[0], PROGRAM.MainWindow.get_size()[1], [200,400], [10,10] )
			
			self.surrounding_loot = inventory.Graphical_Inventory( PROGRAM.MainWindow.get_size()[0], PROGRAM.MainWindow.get_size()[1], [200,180], [10,3] , Dynamic = True )
			
			self.surrounding_loot.draw()
			self.inventory.draw()	
			
			self.keys = pygame.key.get_pressed()
			
			self.seen_tiles = []
			
	def act(self):
		from game import PROGRAM
		
		action = True #in case of
		
		if not self.has_turn:
			if self.AP < self.MAX_AP:
				self.AP += 10 + self.speed
				
		old_tile = self.tile
		#PROGRAM.draw.append(self)

		if self.AP >= 100:
			action = self.behaviour[0](self)
		if not action:
			self.has_turn = True
		else:
			self.has_turn = False
			PROGRAM.draw.append(self.tile)
			if not self.tile == old_tile:
				PROGRAM.draw.append(old_tile)
		return action
		
	def FOW(self): #Only for PC
		vision.player_sigth( self )		
	
	def check_loot(self): #only for PC
		from game import PROGRAM
		self.surrounding_loot.empty_slots()
		tiles = self.tile.next.values()
		tiles.append(self.tile)
		if not PROGRAM.mouse[3] == None:
			loss = True
			for tile in tiles:
				for item in tile.content:
					if item == PROGRAM.mouse[3]: #if item is one currently examined by player
						loss = False
					else:
						self.surrounding_loot.auto_add_item(item)
			if loss:
				if self.inventory.check_if_contains(PROGRAM.mouse[3]):
					loss = False
			if loss:
				PROGRAM.mouse[3] = None
		else:
			for tile in tiles:
					for item in tile.content:
						self.surrounding_loot.auto_add_item(item)
		PROGRAM.draw.append(self.surrounding_loot)
		if self.surrounding_loot.y_offset > self.surrounding_loot.dimensions[1] - (self.surrounding_loot.viewport_size[1]):
			self.surrounding_loot.y_offset = self.surrounding_loot.dimensions[1] - (self.surrounding_loot.viewport_size[1])
							
							
	def controls(self): #Only for PC
		pass
				
	def draw(self):
		self.tile.draw_content(self)	
		
	def move(self,offset):
		
		self.tile.occupier = None
		self.tile.node.objects.remove(self)
		
		success = False
		if (not offset[0] == 0 and not offset[1] == 0):
			cost = self.movcost*1.4
		else:
			cost = self.movcost
		try:
			if not "WALL" in self.tile.next[offset].FLAGS and self.tile.next[offset].occupier == None:
				self.tile = self.tile.next[offset]
				success = True
				self.AP -= cost
			elif cost == self.movcost*1.4:
				test1 = (offset[0] , 0)
				test2 = (0 , offset[1])
				
				if "WALL" not in self.tile.next[test1].FLAGS and "WALL" in self.tile.next[test2].FLAGS:
					if  self.tile.next[test1].occupier == None:
						self.tile = self.tile.next[test1]
						self.AP -= self.movcost
						success = True	
						
				elif "WALL" in self.tile.next[test1].FLAGS and not "WALL" in self.tile.next[test2].FLAGS:
					if  self.tile.next[test2].occupier == None:
						self.tile = self.tile.next[test2]
						self.AP -= self.movcost
						success = True										
		except KeyError:
			pass
			
		self.tile.occupier = self
		self.tile.node.objects.append(self)

		return success
		
#class unit_info(self):
#	p
